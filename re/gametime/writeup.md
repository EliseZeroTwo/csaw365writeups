# Gametime

### Initial Steps
First off I ran the provided executable. You had to press the key it said within a short amount of time. This was probably possible to do by hand but eh RE challange

## Reversing
Putting the binary in my *totally legit* copy of IDA we're greeted with a mess of stuff in main. I decided to peek at the SR's and I found the section that manages fails or successes.
The instructions were 

 

    loc_4014CA:
	mov     edx, 186A0h
	mov     ecx, esi
	call    sub_401260
	pop     edi
	pop     esi
	pop     ebx
	test    al, al
	jnz      short loc_401503
	
loc_401503 being the success

patching that final jnz to a jz, and running the binary and inputting nothing we get the flag after a short period of time.

Here is the final patched binary: https://gofile.me/1YgmO/2w0DlF6a7

Here is the flag: (no5c30416d6cf52638460377995c6a8cf5)



    

